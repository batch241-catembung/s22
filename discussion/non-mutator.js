//console.log("qq");

//non-mutator methods
/*
	are function that do not modify or change an array after they  have been created 
	
	these methods do not manipulate the original array

	these methids perform task such as returning (or getting) elements from an array combining arrays and prinying the output

	indexOf()
	lastIndexOf()
	slice()
	toString()
	concat()
	join()
*/
console.log('%c non-mutator Method:', 'background: #89CFF0')
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

console.log(countries);
console.log("");


//indexOf()
/*
	return the index number of the "firts matching element" found in an array

	if no match hase found the result will be -1

	the search process will be done from the first element proceeding to the last element

	syntax
	arrayName.indexOf(SearchValues)
	arrayName.indexOf(SearchValues, fromIndex)
*/

console.log('%c indexOf Method:', 'background: #89CFF0');
let firstIndex = countries.indexOf('PH');
console.log('result of indexOf '+ firstIndex);

let invalidCountry = countries.indexOf('Brazil');
console.log('result of indexOf '+ invalidCountry);

// lastIndexOf()
/*
	returns the index number of the "last matching item found in array"

	the search process from the last element proceeding to the first element

	syntax
	arrayName.lastIndexOf(SearchValues)
	arrayName.lastIndexOf(SearchValues, fromIndex)
*/

console.log('%c lastIndexOf Method:', 'background: #89CFF0');
let lasrIndex = countries.lastIndexOf('PH');
console.log('result of lastIndexOf '+ lasrIndex);

let invalidCountry2 = countries.indexOf('Brazil');
console.log('result of indexOf '+ invalidCountry2)


//getting the index number of starting from a specified index

let lastIndexStart = countries.lastIndexOf("PH",6);
console.log('result of lastIndexOf '+ lastIndexStart);

let lastIndexStarter = countries.lastIndexOf("PH",4);
console.log('result of lastIndexOfsss '+ lastIndexStarter);


//slice()
/*
	slice lemetns from an array and returns new array
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, EndingIndex);
*/

console.log('%c slice Method:', 'background: #89CFF0');
//slicing of elements from a specified index to the last element 
let slicedArrayA = countries.slice(2);
console.log("return from slice method");
console.log(slicedArrayA);


//slicing off elements from a specified index to another index
console.log(countries);
let slicedArrayB = countries.slice(0, 4);
console.log("return from slice method");
console.log(slicedArrayB);


//slicing off elements starting from the last element of an array
console.log("negative values"); 
 let slicedArrayC = countries.slice(-3);
 let slicedArrayD = countries.slice(-3, -1);
 let slicedArrayE = countries.slice(-5);
console.log("return from slice method"); 
console.log(slicedArrayC);
console.log(slicedArrayD);
console.log(slicedArrayE);


//toString()
/*
	return an array as a string seperated by commas

	syntax
		arrayName.toString()
*/
console.log('%c toString Method:', 'background: #89CFF0');

 let stringArray = countries.toString();
console.log("result of to string method ");
console.log(stringArray);

//concat()
/*
	combines 2 arrays and returns combine result

	syntax
		arrayA.concat.(arrayB);
		arrayA.concat.(elementA, elementB);

*/

console.log('%c concat Method:', 'background: #89CFF0');

 let taskArrayA = ["drink HTML", "eat Javascript"];
 let taskArrayB = ["inhale css", "breath sass"];
 let taskArrayC = ["get git", "be node"];

let showTask = taskArrayA.concat(taskArrayB);
console.log("result from concat method")
console.log(showTask);

//multiple concat array
console.log("result from concat multiple method")
let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTask);
//rearrage the position of array
console.log("rearrange")
let allTaskA = taskArrayB.concat(taskArrayC, taskArrayA)
console.log(allTaskA);


//combining  array with elements 
let combineTask = taskArrayA.concat("smell express","throw react");
console.log(combineTask);
console.log("");

//join 
/*
	returns an array as a string seperated by specified separator string 

	syntax
		arrayName.join("seprator string")
*/
console.log('%c join Method:', 'background: #89CFF0');
let users =['john', 'jane', 'joey', 'robert'];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(' - '));
console.log(users.join(' & '));
console.log("");


console.log('%c end of non-mutator Methods', 'background: #89CFF0');

console.log("");
console.log("");
//-----------------------------------

console.log('%c iteration Methods', 'background: #89CFF0');
//iteration methods
/*
	are loops design to perform repititive task on arrays 

	useful for manipulating array data resulting in complex task
*/

//for each()
/*
	similar to the for loop that iterates on each array elements

	variable names for array are usualy written in a plural form of data stored in a  an array

	it is  common practice to use singular form the array content for parameter names used in array loops 

	array iteration methods noramlly work with function supplied as an argument

	syntax
		arryName.ForEach(function(individualElement){statement});
*/

allTask.forEach(function(task) {
	console.log(task)
});

let students = ['jeru', 'annejanett', 'glyn','jake', 'gab','daniel'];

students.forEach(function(student){
	console.log("hi student: "+student);
});

console.log("")

//using for each with conditional statements
console.log('%c filterd forEach ', 'text-decoration:underline;');


let filterTasks = [];

allTask.forEach(function(task){
	if (task.length >= 10){
		console.log(task);
		filterTasks.push(task);
	}
})

console.log("result offilter tasks");
console.log(filterTasks);

console.log("")


students.forEach(function(student){
	if (student.length <= 4){
		console.log("kanan ng room "+student);
	}else{
		console.log("kaliwa ng room "+student);
	}
})

console.log("")


//map()
/*
	it iterates in each element AND returns new array with different values dependeing on the result of the function operation

	this is usefull for performing task where mutating/changing the elements are required

	unlike the foreach method, the map method requires the use of a "return" statements in order to create another array with the performed opertaion

	syntax
		let resultArray = arrayName.map(function(individualElement))

*/

let numbers = [1,2,3,4,5];
console.log("before the map "+numbers);


let numberMap = numbers.map(function(number){
	return number * number;
})

console.log("result/after of the map");
console.log(numberMap);
console.log("");


//every()
/*
	checks if all element in an array meet the given condition 

	ths us usefu; for validating data stored in arrays especially given delaing with large amount of data

	return the true value if all elemetns meet the condition and false if otherwise

	syntax
	let resultArray = arrayName.every(funtion(element){
	return expression / condition 
	})



*/

let allValid = numbers.every(function(number){
	return (number < 3 );
});

console.log("result of every method");
console.log(allValid);
console.log("");


//some 
/*
	checks if 1 element meet the given condition

	return true value if atleast 1 of the element meet the condition and false otherwise

	syntax
	let resultArray = arrayName.some(function(element){
	return expression 
	})


*/

console.log('%c some()', 'background: #89CFF0');

let someValid = numbers.some(function(number){
	return (number <2);

});

console.log('result of some method');
console.log(someValid);

//combiniong the return result from every same method may be used in other statement to perform consecutive

if (someValid){//truty tables
	console.log('some numbers is greater than 2');
}

console.log("");

//filter()

/*
	returns new array that contains elements which meet the given condition

	return an empty array if no elements are found 

	useful for filtering  array elements with a given condition and shorten the syntax compared to using other array iteration methods
	syntax
		let resultArray = arrayName.filter(function(element){
	return expression
		})
*/

console.log('%c filter()', 'background: #89CFF0');


let filterValid = numbers.filter(function(number){
	return	(number < 3)
})
console.log('original');
console.log(numbers);
console.log('result of filter method');
console.log(filterValid);


// no elements found
let nothingFound = numbers.filter(function(number){
	return (number = 0);
});

console.log(nothingFound);

// filtering using forEach
let filteredNumber = [];
numbers.forEach(function(number){
	console.log(number);
	if (number < 3 ){
		filteredNumber.push(number);
	}
});
console.log("result of filter method");
console.log(filteredNumber);


//includes() method
/*
	methods can be "chained" by using them one after another

	the result of first method is used to second method until all chained method have been solved
*/
console.log('%c includes()', 'background: #89CFF0');

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor']

let filterProducts = products.filter(function(product)
{
	return	product.toLowerCase().includes("a");
})
console.log(filterProducts);
console.log("");


//reduces()
/*
	evaluate statements from left to right and return reduces thje array into a single value
	syntax
	let resultArray = array.Name.reduce(function(accumulator, currentValue){
	return expression
	});
	 
	the accumulator parameter in the function sores the result of ecery iteration of the loop

	teh currentValue is the current next element in the array that is evaluated in each iteration of the loop

*/


/*
	how it works
	1. the first result in the array sores in the accumulator parameter
	2. tehe 2nd elemend in the array is stores in the accumulator parameters
	3.an operation is performed on the 2 elements
	4. the loop repeats step 1 to 3 until all the elements have been worked on 
*/


let iteration = 0;
let reduceArray = numbers.reduce(function(x, y){
	console.warn("current iteration"+ ++iteration);
	console.log("accumulator "+ x);
	console.log("current value " + y);
	//operation to reduce the array into a single value
	return x + y;
})

console.log("current iteration "+reduceArray);

//reducing string arrays

let list=['hello', 'again', 'world'];
let reduceJoin = list.reduce(function(x, y){
	return x + '-'+ y;
});

console.log("result: "+reduceJoin);









