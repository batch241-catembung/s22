//console.log("hello world");

//array methods (2 types)
//mutator and none mutator

//mutators methods

/*
	are function that "mutate" or change an array after they're created

	these methods manipulates the original array. performing task such as adding adn removing elements
	
	push()
	pop()
	ushift()
	shift()
	splice()
	sort()
	reverse()
*/
console.log("%c mutateor methods", 'font-weight: bold')
let fruits =['apple', 'orange', 'kiwi', 'dragon fruit'];
console.log(fruits);
console.log("");

//push
/*
	add in the end of the array and returns the array length
	[0,0,"x"]
	syntax:
	arrayName.push()
*/

console.log("push method");
let fruitsLength = fruits.push("mango");
console.log(fruitsLength);
console.log("mutated array from push method");
console.log(fruits);

//push multiple elements to array
fruits.push("avocado", "guava");
console.log("mutated array: multiple values");
console.log(fruits);
console.log("");

//pop()
/*
	remove the last element and returns the remove element
	[0, 0, "x"]
	syntax
		arrayName.pop();
*/

console.log("pop method");
let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("mutated array freom pop method");
console.log(fruits);
console.log("");

//unshift 
/*
	adds at the start of an array
	["x", 0, 0]
	syntax
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');

*/
console.log('%c Unshift Method:', 'background: #89CFF0');
fruits.unshift('lime', 'banana');
// let trialUnshift = fruits.unshift();
// console.log(trialUnshift);
console.log("mutated array from unshift method");
console.log(fruits);
console.log("");

//shift 

/*
	remove elements at the start of an array and returns the remved element 
	syntax
		arrayName.shift();
*/
console.log('%c shift Method:', 'background: #89CFF0')
let anotherfruit = fruits.shift()
console.log(anotherfruit);
console.log("mutated array from unshift method");
console.log(fruits);
console.log("");


//splice()
/*
	simuntaneousley removes elemen from a specified number adn adds elements 
	sytax
		arrayName.splice(startingIndex, deleteCount, elementsToAdd);

	arrayName.splicer (whereToStart, ToDelete, numbersOfItemsToBeDeleted, ElementToBeAdded)
*/

console.log('%c splice Method:', 'background: #89CFF0')
fruits.splice(1,2, 'lime', 'cherry');
console.log("mutated array from splice method");
console.log(fruits);
console.log("");

//sort()
/*
	rearrage the array elements in a alphanumeric order
	syntax
	arrayName.sort()
*/

console.log('%c sort Method:', 'background: #89CFF0')
fruits.sort();
console.log("mutated array from sort method");
console.log(fruits);
console.log("");

// reverse()
/*
	reverses the order of array elements
	syntax
		arrayName.reverse()
*/
console.log('%c sort Method:', 'background: #89CFF0')
fruits.reverse();
console.log("mutated array from reverse method");
console.log(fruits);
console.log("");
console.warn("--end of mutator method")









